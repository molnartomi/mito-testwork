<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180616_101324_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(32)->notNull(),
            'firstname' => $this->string(32)->notNull(),
            'phone' => $this->string(9)->notNull(),
            'birth_date' => $this->date()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
