<?php

use yii\db\Migration;

/**
 * Handles adding edm to table `users`.
 */
class m180617_120822_add_edm_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'edm', $this->boolean().' AFTER `email`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'edm');
    }
}
