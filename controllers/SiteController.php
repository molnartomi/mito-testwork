<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\RegistrationForm;
use app\models\Users;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'registration', 'welcome'],
                'rules' => [
                    [
                        'actions' => ['index', 'registration', 'welcome'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegistration()
    {
        $user = new Users();
        $model = new RegistrationForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->saveUser()) {
            Yii::$app->session->setFlash('success', 'Sikeres regisztáció');
            return $this->redirect(['welcome']);
        }
        return $this->render('registration', [
            'model' => $model,
            'years' => RegistrationForm::getYearOptions(),
            'months' => RegistrationForm::getMonthOptions(),
            'days' => RegistrationForm::getDayOptions(),
        ]);
    }

    public function actionWelcome()
    {
        return $this->render('welcome');
    }
}
