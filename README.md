### Install

1. clone project

    ```
    git clone https://gitlab.com/molnartomi/mito-testwork.git
    ```

2. update vendor packages

    ```
   docker-compose run --rm php composer update --prefer-dist
   ```

3. run yii migrations 

    ```
    docker-compose run --rm php yii migrate 
    ```

4. start the containers

    ```
    docker-compose up -d
    ```

5. access the application
    ```
    http://localhost:7000
    ```