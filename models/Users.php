<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $surname
 * @property string $firstname
 * @property string $phone
 * @property string $birth_date
 * @property string $email
 * @property int $edm
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['surname', 'firstname', 'phone', 'birth_date', 'email', 'edm'], 'required'],
            [['birth_date', 'created_at', 'updated_at'], 'safe'],
            [['edm'], 'integer'],
            [['surname', 'firstname'], 'string', 'max' => 32],
            [['phone'], 'string', 'max' => 9],
            [['email'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Surname',
            'firstname' => 'Firstname',
            'phone' => 'Phone',
            'birth_date' => 'Birth Date',
            'email' => 'Email',
            'edm' => 'Edm',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
