<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $surname;
    public $firstname;
    public $phone;
    public $email;
    public $birthYear;
    public $birthMonth;
    public $birthDay;
    public $gameTerms;
    public $edm;
    private $user;

    public function __construct(Users $user, $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthYear', 'birthMonth', 'birthDay'], 'required', 'message' => 'Kötelező mező'],

            [['surname'], 'required', 'message' => 'Vezetéknév nem lehet üres.'],
            [['firstname'], 'required', 'message' => 'Keresztnév nem lehet üres.'],
            [['surname', 'firstname'], 'string', 'max' => 32],

            [['phone'], 'required', 'message' => 'Telefonszám nem lehet üres.'],
            [['phone'], 'string', 'max' => 9],

            [['email'], 'string', 'max' => 255],
            [['email'], 'required', 'message' => 'E-mail nem lehet üres.'],
            [['email'], 'unique', 'targetClass' => Users::class],

            ['gameTerms', 'required', 'requiredValue' => 1, 'message' => 'El kell fogadnod a játékszabályzatot!'],
            ['edm', 'boolean', 'trueValue' => true, 'falseValue' => false, ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'surname' => 'VEZETÉKNÉV',
            'firstname' => 'KERESZTNÉV',
            'phone' => 'TELEFONSZÁM',
            'birthYear' => 'Birth Date',
            'birthMonth' => 'Birth Date',
            'birthDay' => 'Birth Date',
            'email' => 'E-MAIL',
            'gameTerms' => 'Elfogadom a Játékszabályzatot.',
            'edm' => 'Hozzájárulok a személyre szabott ajánlatok kidolgozása és ajánlattétel céljából történő adatkezeléshez',
        ];
    }

    public static function getYearOptions(): array
    {
        $years = range(1900, 2000);
        return array_reverse(array_combine($years, $years), true);
    }

    public static function getMonthOptions(): array
    {
        return [
            '01' => 'Január',
            '02' => 'Február',
            '03' => 'Március',
            '04' => 'Április',
            '05' => 'Május',
            '06' => 'Június',
            '07' => 'Július',
            '08' => 'Augusztus',
            '09' => 'Szeptember',
            '10' => 'Október',
            '11' => 'November',
            '12' => 'December',

        ];
    }

    public static function getDayOptions(): array
    {
        $days = range(1, 31);
        return array_combine($days, $days);
    }

    public function saveUser(): bool
    {
        $user = $this->user;
        $user->surname = $this->surname;
        $user->firstname = $this->firstname;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->edm = $this->edm;
        $user->birth_date = $this->birthYear.'-'.$this->birthMonth.'-'.$this->birthDay;
        return $user->save(true);
    }
}
