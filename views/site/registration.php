<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RegistrationForm */
/* @var $form ActiveForm */
?>
<h3>Először is add meg az adataidat!</h3>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <?= $form->field($model, 'surname')->textInput() ?>
    <?= $form->field($model, 'firstname')->textInput() ?>
</div>
<div class="row">
    <?= $form->field($model, 'phone')->textInput() ?>
    <div class="required">
        <?= Html::label('SZÜLETÉSI DÁTUM', '', ['class' => 'control-label']) ?>
        <div class="row row--column-3">
            <?= $form->field($model, 'birthYear', ['template' => '<div class="custom-select">{input}</div>{error}'])->dropDownList(
                $years,
                ['prompt' => 'Év']
            )->label(false); ?>
            <?= $form->field($model, 'birthMonth', ['template' => '<div class="custom-select">{input}</div>{error}'])->dropDownList(
                $months,
                ['prompt' => 'Hónap']
            )->label(false); ?>
            <?= $form->field($model, 'birthDay', ['template' => '<div class="custom-select">{input}</div>{error}'])->dropDownList(
                $days,
                ['prompt' => 'Nap']
            )->label(false); ?>
        </div>
    </div>
</div>
<div class="row">
    <?= $form->field($model, 'email')->textInput() ?>
</div>
<?= $form->field($model, 'edm', ['template' => '{input}{label}{error}'])->checkbox([], false) ?>
<?= $form->field($model, 'gameTerms', ['template' => '{input}{label}{error}'])->checkbox([], false) ?>
<div class="section--quiz--hint">
    <p>Amennyiben legközelebb is velünk játszol, már nem kell regisztrálnod újra. A beazonosításhoz elegendő lesz megadnod az e-mail címed és még egy adatot.</p>
</div>
<div class="text--center">
    <p>
        <strong>A Tovább gomb megnyomásával kijelentem, hogy az <a href="#" target="_blank">Adatkezelési tájékoztató</a> tartalmát megismertem és tudomásul vettem</strong>
    </p>
</div>
<div class="text--center send-button">
    <?= Html::submitButton('Tovább', ['class' => 'btn btn--primary']) ?>
    <?= Html::a('Vissza', '/') ?>
</div>
<?php ActiveForm::end(); ?>

