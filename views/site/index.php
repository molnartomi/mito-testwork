<?php

/* @var $this yii\web\View */

$this->title = 'Főoldal';
?>

<div class="card">
    <div class="card__inner">
        <h3>Fizess a mobiloddal, ha nincs kéznél a kártyád, tárcád!</h3>
        <div class="group">
            <div class="section__about__video">
                <div class="section__about__videowrapper">
                    <iframe width="460" height="257" src="https://www.youtube.com/embed/V9i_9hXdQQw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="section__about--left">
                <p>Telefon sokszor akkor is van nálunk, amikor semmi mást nem viszünk magunkkal.
                    A Simple by OTP alkalmazás segítségével most már, akkor is fizethetünk, ha nincs nálunk a pénztárcánk. Mostantól a mobil az új pénztárca!
                    Töltsd le a Simple by OTP alkalmazást, és fizess mobiloddal gyorsan és egyszerűen</p>
                <ul class="list list--checked">
                    <li class="list__item">futás közben,</li>
                    <li class="list__item">kiránduláskor,</li>
                    <li class="list__item">moziban,</li>
                    <li class="list__item">útközben.</li>
                </ul>
            </div>
        </div>
        <p>
            <b>Ráadásul akkor is fizethetsz a mobiloddal, ha nem az OTP Bank ügyfele vagy.</b>
            <a id="js-modal-open" class="link link--blue" href="#" data-ua="1389005206" target="_blank" data-mote-type="visit.tooltip" data-mote-value="fizetes_mobillal_nem_otp">Nézd meg, hogyan!</a>
        </p>
        <a href="https://play.google.com/store/apps/details?id=com.otpmobil.simple&amp;referrer=utm_source%3Dotpmindennap.hu%252Fmobilfizetes%26utm_campaign%3Dotpsimple_2018_tavasz" data-ua="1249471003" target="_blank" data-mote-type="visit.external" data-mote-value="https://play.google.com/store/apps/details?id=com.otpmobil.simple&amp;referrer=utm_source%3Dotpmindennap.hu%252Fmobilfizetes%26utm_campaign%3Dotpsimple_2018_tavasz">
            <img class="breadcrumbs__img" src="<?= Yii::getAlias('@web').'/img/logo-googleplay.png' ?>" alt="">
        </a>
    </div>
</div>

