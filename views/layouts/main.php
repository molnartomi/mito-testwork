<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header id="header">
</header>
<main class="main">
    <section class="hero">
        <div class="hero__mobile"></div>
        <div class="hero__container wrapper">
            <h1><a href="<?= Yii::$app->homeUrl; ?>" title="OTP Bank">OTP Bank</a></h1>
            <header class="hero__heading">
                <h2 class="hero__title hero__title--main">
                    A mobil az új pénztárca
                </h2>
                <h3 class="hero__title hero__title--sub" data-animation-delay="300">
                    Fizess a mobiloddal, tippelj, hogy nyerhess.
                </h3>
            </header>
            <?php if (Yii::$app->request->getUrl() != Yii::getAlias('@reg')): ?>
            <footer class="hero__footer" data-animation-delay="600">
                <?= Html::a('Szeretnék nyerni', Yii::getAlias('@reg'), ['class' => 'btn btn--orange']) ?>
            </footer>
            <?php endif; ?>
            <div class="wrapper">
                <ul class="progress-bar">
                    <li class="progress-bar__item progress-bar__item--active"><span></span></li>
                    <li class="progress-bar__item "><span></span></li>
                    <li class="progress-bar__item "><span></span></li>
                </ul>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="wrapper">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </section>
</main>
<footer class="footer">
    <div class="wrapper">
        <a href="http://phptestwork.staging.mito.hu/" class="logo" title="OTP Bank">OTP Bank</a>
        <nav>
            <ul>
                <li>
                    <a href="https://www.otpbank.hu/portal/hu/Kapcsolat" data-ua="3277595567" data-mote-type="visit.external" data-mote-value="https://www.otpbank.hu/portal/hu/Kapcsolat" target="_blank">Kapcsolat</a>
                </li>
                <li>
                    <a href="#" data-ua="1382196950" data-mote-type="visit.item" data-mote-value="reszveteli_szabalyzat" target="_blank">Játékszabályzat</a>
                </li>
                <li>
                    <a href="#" data-ua="2537657349" data-mote-type="visit.item" data-mote-value="adatkezelesi_nyilatkozat_elemzesi_celu" target="_blank">Adatkezelési nyilatkozat</a>
                </li>
                <li>
                    <a href="/segitseg" data-ua="1897517446" data-mote-type="visit.item" data-mote-value="elakadtal">Elakadtál?</a>
                </li>
                <li>
                    <a class="open-cookie-modal" href="/fbfrontend/default/#" data-ua="3674042236">Cookie Beállítások</a>
                </li>
            </ul>
            <aside>
                © Copyright 2018. OTP Bank<span>A Google Play és a Google Play-logó a Google Inc. védjegyei.</span>
            </aside>
        </nav>
        <aside>
            <i class="icon icon--mastercard"></i>
            <i class="icon icon--maestro"></i>
        </aside>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
